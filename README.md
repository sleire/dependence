# lgportf

Portfolio Allocation under Asymmetric Dependence in Asset Returns using
Local Gaussian Correlations

### Directory structure

    ## .
    ## ├── Data
    ## │   ├── 17_Industry_Portfolios.CSV
    ## │   └── Indices_6.csv
    ## ├── R
    ## │   ├── config.R
    ## │   └── functionlib.R
    ## ├── README.md
    ## ├── README.Rmd
    ## ├── Results
    ## │   ├── Indices_6
    ## │   │   ├── Descriptive
    ## │   │   ├── Figures
    ## │   │   └── Output
    ## │   └── Industry_17
    ## │       ├── Descriptive
    ## │       ├── Figures
    ## │       └── Output
    ## └── run_pipeline.R
    ## 
    ## 11 directories, 7 files

### Bring your own data

1.  Put my\_data.csv in “Data” directory
2.  Create “my\_analysis” folder in “Results” with sub folders
    “Descriptive”, “Figures” and “Output”
3.  Update config.R
4.  run\_pipeline.R
