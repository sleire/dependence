library(progress)
library(Matrix)
library(xts)
library(lg)
library(PortfolioAnalytics)
library(PerformanceAnalytics)
library(stargazer)
library(ROI)
library(ROI.plugin.quadprog)
library(data.table)
library(ggplot2)
library(PeerPerformance)
#library(corpcor)

# descriptive statistics

calc_summary <- function(ret){
  dat <- rbind(apply(ret, 2, length), apply(ret, 2, mean), apply(ret, 2, sd), apply(ret, 2, var))
  Skewness <- timeSeries::colSkewness(ret)
  Kurtosis <- timeSeries::colKurtosis(ret)
  Jarque.Bera <- sapply(1:dim(ret)[2], function(i) tseries::jarque.bera.test(ret[,i])$statistic)
  SharpeRatio <- PerformanceAnalytics::SharpeRatio(ret, FUN = "StdDev")
  Max.down <- PerformanceAnalytics::maxDrawdown(ret/100)*100
  
  dat <- rbind(dat, Skewness, Kurtosis, Jarque.Bera, SharpeRatio, Max.down, apply(ret, 2, quantile))
  rownames(dat) <- c("Observations", "Mean", "Std.dev.", "Variance",
                     "Skewness", "Kurtosis", "Jarque-Bera", "Sharpe ratio", "Max. drawdown",
                     "Min", "1 Quartile", "Median", "3 Quartile", "Max")
  round(dat, 4)
}

# moving window calculations

get_windows <- function(ret, win_size, start_idx, end_idx){
  if (start_idx < win_size)
    stop("start_idx cannot be smaller than win_size")
  
  if (end_idx < start_idx)
    stop("end_idx cannot be smaller than start_idx")
  
  if (dim(ret)[1] < win_size)
    stop("win_size cannot be greater than input matrix")
  
  win_list <- list()
  list_idx <- 1
  for (i in (start_idx):end_idx){
    win_list[[list_idx]] <-  window(ret, start = time(ret)[i - win_size + 1], 
                                    end = time(ret)[i])
    
    list_idx <- list_idx + 1
  }
  win_list
}

get_win_cov <- function(win_list){
  cov_list <- list()
  for (i in 1:length(win_list)){
    cov_list[[i]] <- cov(win_list[[i]])
  }
  cov_list
}

get_win_lg_obj_bw_adj <- function(win_list, bw_adj = 1.1){
  
  ncols = dim(win_list[[1]])[2]
  
  # progress bar
  pb <- progress_bar$new(
    format = "  Running [:bar] :percent eta: :eta",
    total = length(win_list), clear = FALSE, width= 60)
  
  # create pairs matrix
  pairs_mat <- matrix(ncol = 2, nrow = 0)
  for (c in 1:(ncols-1)){
    pairs_mat <- rbind(pairs_mat, expand.grid(c,(c+1):ncols))
  }
  pairs_mat <- as.matrix(pairs_mat)
  num_pairs <- dim(pairs_mat)[1]
  
  lg_obj_list <- list()
  for (i in 1:length(win_list)){
    # matrix from window i in win_list
    mat <- win_list[[i]]
    
    # list to store the lg_obj for all pairs in matrix for window i
    pair_list <- list()
    
    # step through pairs in matrix (same dim or all matrices)
    for (p in 1:num_pairs){
      # local correlations for pair (xy)  number p
      xy <- mat[, c(pairs_mat[p, 1], pairs_mat[p, 2])]
      lg_ini <- lg_main(xy, 
                        bw_method="plugin", 
                        est_method = "1par",
                        transform_to_marginal_normality = TRUE)
      # adjustments
      lg_ini$bw$joint[,3] = bw_adj * sd(xy[,1])
      lg_ini$bw$joint[,4] = bw_adj * sd(xy[,2])
      
      lg_fin <- lg_main(xy, 
                        bw=lg_ini$bw, 
                        est_method = "5par",
                        transform_to_marginal_normality = FALSE)
      
      pair_list[[p]] <- lg_fin
    }
    # add pair_list with lg_objects for matrix i to main lg_obj_list
    lg_obj_list[[i]] <- pair_list
    # increment progress bar
    pb$tick()
  }
  lg_obj_list
}

get_win_lcov <- function(lg_obj_list, ncols, ass_names, win_tail, global_sd = FALSE){
  
  # progress bar
  pb <- progress_bar$new(
    format = "  Running [:bar] :percent eta: :eta",
    total = length(lg_obj_list), clear = FALSE, width= 60)
  
  # list for storing lcov's from each window
  lcov_list <- list()
  
  # create pairs matrix
  pairs_mat <- matrix(ncol = 2, nrow = 0)
  for (c in 1:(ncols-1)){
    pairs_mat <- rbind(pairs_mat, expand.grid(c,(c+1):ncols))
  }
  pairs_mat <- as.matrix(pairs_mat)
  num_pairs <- dim(pairs_mat)[1]
  
  items <- length(lg_obj_list)
  
  # step through items in the lists
  for (i in 1:items){
    
    # local covariance matrix to be populated for window i
    lcov <- matrix(nrow = ncols, ncol = ncols)
    
    # step through pairs in matrix (same dim for all matrices)
    for (p in 1:num_pairs){
      # current pair
      pair <- pairs_mat[p,]
      
      # lg_obj for pair p in window i
      lg_obj <- lg_obj_list[[i]][[p]]
      
      # select point with moving average, use win_tail = 1 for only last obs in window
      x <- mean(tail(lg_obj$x, win_tail)[,1]) # get the data from the lg_obj
      y <- mean(tail(lg_obj$x, win_tail)[,2])
      dens_est <- dlg(lg_obj, grid = rbind(c(x, y), c(x, y)*1.1))
      loc_cor <- dens_est$loc_cor[1,1]
      
      # calculate lcov with global or local sd's
      if (global_sd){
        x_sd <- sd(lg_obj$x[,1])
        y_sd <- sd(lg_obj$x[,2])
      } else {
        x_sd <- dens_est$loc_sd[1,1] # verify pos
        y_sd <- dens_est$loc_sd[1,2] # verify pos
      }
      
      # add the elements to local covariance matrix
      lcov[pair[1], pair[2]] <- loc_cor*x_sd*y_sd
      lcov[pair[2], pair[1]] <- loc_cor*x_sd*y_sd
      lcov[pair[1], pair[1]] <- x_sd**2
      lcov[pair[2], pair[2]] <- y_sd**2
    }
    dimnames(lcov) <- list(ass_names, ass_names)
    lcov_list[[i]] <- lcov
    
    # increment progress bar
    pb$tick()
  }
  lcov_list
}

get_pos_semdef_mat <- function(win_lcov_list){
  lcov_list <- list()
  for (i in 1:length(win_lcov_list)){
    lcov_list[[i]] <- as.matrix(Matrix::nearPD(win_lcov_list[[i]])$mat)
  }
  lcov_list
}

get_win_lcor_r_quantile <- function(lg_obj_list, ncols, ass_names, r_quantile){
  
  # progress bar
  pb <- progress_bar$new(
    format = "  Running [:bar] :percent eta: :eta",
    total = length(lg_obj_list), clear = FALSE, width= 60)
  
  # list for storing lcor's from each window
  lcor_list <- list()
  
  # create pairs matrix
  pairs_mat <- matrix(ncol = 2, nrow = 0)
  for (c in 1:(ncols-1)){
    pairs_mat <- rbind(pairs_mat, expand.grid(c,(c+1):ncols))
  }
  pairs_mat <- as.matrix(pairs_mat)
  num_pairs <- dim(pairs_mat)[1]
  
  items <- length(lg_obj_list)
  
  # step through items in the lists
  for (i in 1:items){
    
    # local correlation matrix to be populated for window i
    lcor <- matrix(nrow = ncols, ncol = ncols)
    
    # step through pairs in matrix (same dim for all matrices)
    for (p in 1:num_pairs){
      # current pair
      pair <- pairs_mat[p,]
      
      # lg_obj for pair p in window i
      lg_obj <- lg_obj_list[[i]][[p]]
      
      # find grid point corresponding to r_quantile for each asset in pair
      grid <- matrix(apply(lg_obj$x, 2, function(x) quantile(x, r_quantile)), ncol = 2)
      
      dens_est <- dlg(lg_obj, grid = grid)
      loc_cor <- dens_est$loc_cor[1,1]
      
      # add the elements to local correlation matrix
      lcor[pair[1], pair[2]] <- loc_cor
      lcor[pair[2], pair[1]] <- loc_cor
      lcor[pair[1], pair[1]] <- 1
      lcor[pair[2], pair[2]] <- 1
    }
    dimnames(lcor) <- list(ass_names, ass_names)
    lcor_list[[i]] <- lcor
    
    # increment progress bar
    pb$tick()
  }
  lcor_list
}


# portfolio allocations

# global covariance matrix

get_mvs_w_gc <- function(ret, win_size, start_idx, end_idx, min_box, max_box){ #, trans_cost){
  
  weigth_mat <- matrix(ncol = dim(ret)[2], nrow = 0)
  
  # step through windows
  for (i in start_idx:(end_idx)){
    
    returns <- window(ret, start = time(ret)[i - win_size + 1], 
                      end = time(ret)[i])
    
    mvs_gc <- portfolio.spec(colnames(returns))
    mvs_gc <- add.constraint(portfolio = mvs_gc, type = "full_investment")
    mvs_gc <- add.objective(portfolio = mvs_gc, type = "return", name = "mean", target=mean(returns))
    mvs_gc <- add.objective(portfolio = mvs_gc, type = "risk", name = "StdDev")
    mvs_gc <- add.constraint(portfolio = mvs_gc, type = "box", min = min_box, max = max_box)
    
    #mvs_gc <- add.constraint(portfolio = mvs_gc, type = "transaction_cost", ptc = trans_cost)
    
    mvs_gc.opt <- optimize.portfolio(returns, portfolio = mvs_gc, optimize_method= "ROI")
    weigth_mat <- rbind(weigth_mat, as.numeric(PortfolioAnalytics::extractWeights(mvs_gc.opt)))
    
  }
  colnames(weigth_mat) <- colnames(ret)
  # TODO: change hard coded frequency=12
  ts(weigth_mat, start = time(ret)[start_idx],  frequency = 12)
}

get_mvsc_w_gc <- function(ret, win_size, start_idx, end_idx){
  
  weigth_mat <- matrix(ncol = dim(ret)[2], nrow = 0)
  
  # step through windows
  for (i in start_idx:(end_idx)){
    
    returns <- window(ret, start = time(ret)[i - win_size + 1], 
                      end = time(ret)[i])
    
    mvsc_gc <- portfolio.spec(colnames(returns))
    mvsc_gc <- add.constraint(portfolio = mvsc_gc, type = "full_investment")
    mvsc_gc <- add.objective(portfolio = mvsc_gc, type = "return", name = "mean", target=mean(returns))
    mvsc_gc <- add.objective(portfolio = mvsc_gc, type = "risk", name = "StdDev")
    mvsc_gc <- add.constraint(portfolio = mvsc_gc, type = "long_only")
    mvsc_gc.opt <- optimize.portfolio(returns, portfolio = mvsc_gc, optimize_method= "ROI")
    weigth_mat <- rbind(weigth_mat, as.numeric(PortfolioAnalytics::extractWeights(mvsc_gc.opt)))
    
  }
  colnames(weigth_mat) <- colnames(ret)
  ts(weigth_mat, start = time(ret)[start_idx],  frequency = 12)
}

get_min_w_gc <- function(ret, win_size, start_idx, end_idx, min_box, max_box){
  
  weigth_mat <- matrix(ncol = dim(ret)[2], nrow = 0)
  
  # step through windows
  for (i in start_idx:(end_idx)){
    
    returns <- window(ret, start = time(ret)[i - win_size + 1], 
                      end = time(ret)[i])
    
    min_gc <- portfolio.spec(colnames(returns))
    min_gc <- add.constraint(portfolio = min_gc, type = "full_investment")
    min_gc <- add.objective(portfolio = min_gc, type = "risk", name = "StdDev")
    min_gc <- add.constraint(portfolio = min_gc, type = "box", min = min_box, max = max_box)
    min_gc.opt <- optimize.portfolio(returns, portfolio = min_gc, optimize_method= "ROI")
    weigth_mat <- rbind(weigth_mat, as.numeric(PortfolioAnalytics::extractWeights(min_gc.opt)))
    
  }
  colnames(weigth_mat) <- colnames(ret)
  ts(weigth_mat, start = time(ret)[start_idx],  frequency = 12)
}

get_minc_w_gc <- function(ret, win_size, start_idx, end_idx){
  
  weigth_mat <- matrix(ncol = dim(ret)[2], nrow = 0)
  
  # step through windows
  for (i in start_idx:(end_idx)){
    
    returns <- window(ret, start = time(ret)[i - win_size + 1], 
                      end = time(ret)[i])
    
    minc_gc <- portfolio.spec(colnames(returns))
    minc_gc <- add.constraint(portfolio = minc_gc, type = "full_investment")
    minc_gc <- add.constraint(portfolio = minc_gc, type = "long_only")
    minc_gc <- add.objective(portfolio = minc_gc, type = "risk", name = "StdDev")
    minc_gc.opt <- optimize.portfolio(returns, portfolio = minc_gc, optimize_method= "ROI")
    weigth_mat <- rbind(weigth_mat, as.numeric(PortfolioAnalytics::extractWeights(minc_gc.opt)))
    
  }
  colnames(weigth_mat) <- colnames(ret)
  ts(weigth_mat, start = time(ret)[start_idx],  frequency = 12)
}

# local covariance matrices

get_mvs_w_lc <- function(ret, win_size, start_idx, end_idx, win_lcov_list, min_box, max_box){
  
  weigth_mat <- matrix(ncol = dim(ret)[2], nrow = 0)
  
  # step through windows
  for (i in start_idx:(end_idx)){
    
    returns <- window(ret, start = time(ret)[i - win_size + 1], 
                      end = time(ret)[i])
    
    # add local covariance matix
    momentargs <- list()
    momentargs$sigma <- win_lcov_list[[i - win_size + 1]]
    #momentargs$sigma <- as.matrix(win_lcov_list[[i - win_size + 1]])
    
    mvs_lc <- portfolio.spec(colnames(returns))
    mvs_lc <- add.constraint(portfolio = mvs_lc, type = "full_investment")
    mvs_lc <- add.objective(portfolio = mvs_lc, type = "return", name = "mean", target=mean(returns))
    mvs_lc <- add.objective(portfolio = mvs_lc, type = "risk", name = "StdDev")
    mvs_lc <- add.constraint(portfolio = mvs_lc, type = "box", min = min_box, max = max_box)
    mvs_lc.opt <- optimize.portfolio(returns, portfolio = mvs_lc, optimize_method= "ROI", momentargs = momentargs)
    weigth_mat <- rbind(weigth_mat, as.numeric(PortfolioAnalytics::extractWeights(mvs_lc.opt)))
    
  }
  colnames(weigth_mat) <- colnames(ret)
  ts(weigth_mat, start = time(ret)[start_idx],  frequency = 12)
}

get_mvsc_w_lc <- function(ret, win_size, start_idx, end_idx, win_lcov_list){
  
  weigth_mat <- matrix(ncol = dim(ret)[2], nrow = 0)
  
  # step through windows
  for (i in start_idx:(end_idx)){
    
    returns <- window(ret, start = time(ret)[i - win_size + 1], 
                      end = time(ret)[i])
    
    # add local covariance matix
    momentargs <- list()
    momentargs$sigma <- as.matrix(win_lcov_list[[i - win_size + 1]])
    
    mvsc_lc <- portfolio.spec(colnames(returns))
    mvsc_lc <- add.constraint(portfolio = mvsc_lc, type = "full_investment")
    mvsc_lc <- add.objective(portfolio = mvsc_lc, type = "return", name = "mean", target=mean(returns))
    mvsc_lc <- add.objective(portfolio = mvsc_lc, type = "risk", name = "StdDev")
    mvsc_lc <- add.constraint(portfolio = mvsc_lc, type = "long_only")
    mvsc_lc.opt <- optimize.portfolio(returns, portfolio = mvsc_lc, optimize_method= "ROI", momentargs = momentargs)
    weigth_mat <- rbind(weigth_mat, as.numeric(PortfolioAnalytics::extractWeights(mvsc_lc.opt)))
    
  }
  colnames(weigth_mat) <- colnames(ret)
  ts(weigth_mat, start = time(ret)[start_idx],  frequency = 12)
}

get_min_w_lc <- function(ret, win_size, start_idx, end_idx, win_lcov_list, min_box, max_box){
  
  weigth_mat <- matrix(ncol = dim(ret)[2], nrow = 0)
  
  # step through windows
  for (i in start_idx:(end_idx)){
    
    returns <- window(ret, start = time(ret)[i - win_size + 1], 
                      end = time(ret)[i])
    
    # add local covariance matix
    momentargs <- list()
    momentargs$sigma <- as.matrix(win_lcov_list[[i - win_size + 1]])
    
    min_lc <- portfolio.spec(colnames(returns))
    #min_lc <- add.constraint(portfolio = min_lc, type = "full_investment") # relax full inv 
    min_lc <- add.constraint(portfolio = min_lc, type = "weight_sum", min_sum = 0.99, max_sum = 1.01) # relax full inv 
    min_lc <- add.objective(portfolio = min_lc, type = "risk", name = "StdDev")
    min_lc <- add.constraint(portfolio = min_lc, type = "box", min = min_box, max = max_box)
    
    # include local covariance matrix in optimization
    min_lc.opt <- optimize.portfolio(returns, portfolio = min_lc, optimize_method= "ROI", momentargs = momentargs)
    weigth_mat <- rbind(weigth_mat, as.numeric(PortfolioAnalytics::extractWeights(min_lc.opt)))
    
  }
  colnames(weigth_mat) <- colnames(ret)
  ts(weigth_mat, start = time(ret)[start_idx],  frequency = 12)
}

get_minc_w_lc <- function(ret, win_size, start_idx, end_idx, win_lcov_list){
  
  weigth_mat <- matrix(ncol = dim(ret)[2], nrow = 0)
  
  # step through windows
  for (i in start_idx:(end_idx)){
    
    returns <- window(ret, start = time(ret)[i - win_size + 1], 
                      end = time(ret)[i])
    
    # add local covariance matix
    momentargs <- list()
    momentargs$sigma <- as.matrix(win_lcov_list[[i - win_size + 1]])
    
    minc_lc <- portfolio.spec(colnames(returns))
    #minc_lc <- add.constraint(portfolio = minc_lc, type = "full_investment")
    minc_lc <- add.constraint(portfolio = minc_lc, type = "weight_sum", min_sum = 0.99, max_sum = 1.01) # relax full inv 
    minc_lc <- add.constraint(portfolio = minc_lc, type = "long_only")
    minc_lc <- add.objective(portfolio = minc_lc, type = "risk", name = "StdDev")
    minc_lc.opt <- optimize.portfolio(returns, portfolio = minc_lc, optimize_method= "ROI", momentargs = momentargs)
    weigth_mat <- rbind(weigth_mat, as.numeric(PortfolioAnalytics::extractWeights(minc_lc.opt)))
    
  }
  colnames(weigth_mat) <- colnames(ret)
  ts(weigth_mat, start = time(ret)[start_idx],  frequency = 12)
}

# > pspec <- add.constraint(portfolio=pspec, type="transaction_cost", ptc=0.01)

eval_weights <- function(weight_list){
  wl <- sapply(weight_list, function(x) which(is.na(x), arr.ind = TRUE))
  eval_list <- list()
  for (i in 1:length(weight_list)){
    
    
    eval_list[[i]] <- which(is.na(weight_list[[i]]), arr.ind = TRUE)
  }
  eval_list
}

get_tcret_or_turnover <- function(ret_port, trans_cost, tcret = TRUE){
  
  # get beggining-of period and en-of-period weights (requires ret_port from PerformanceAnalytics::Return.portfolio() with "verbose=TRUE")
  trans <- ret_port$BOP.Weight - lag(ret_port$EOP.Weight)
  port_turnover <- xts(rowSums(abs(trans)), order.by = index(trans))
  
  # returns after transaction costs
  r_tc <- ret_port$returns + port_turnover * -1 * trans_cost
  
  # return turnover or returns after tcosts
  if(tcret){
    out <- r_tc
  } else {
    out <- port_turnover
  }
  
  out
}

 
# custom performance plot function baseed on PerformanceAnalytics::charts.PerformanceSummary()
cust_charts.PerformanceSummary <-
  function (R, 
            R_colnames,
            Rf = 0, 
            main = NULL, 
            geometric=TRUE, 
            methods = "none", 
            width = 0, 
            event.labels = NULL, 
            ylog = FALSE, 
            wealth.index = TRUE, 
            gap = 12, 
            begin=c("first","axis"), 
            legend.loc="topleft", 
            p=0.95,
            plot.engine="default",
            ...)
  { 
    
            colnames(R) <- R_colnames
    
            begin = begin[1]
            x = checkData(R)
            colnames = colnames(x)
            ncols = ncol(x)
            
            length.column.one = length(x[,1])
            # find the row number of the last NA in the first column
            start.row = 1
            start.index = 0
            while(is.na(x[start.row,1])){
              start.row = start.row + 1
            }
            x = x[start.row:length.column.one,]
            
            if(ncols > 1)
              legend.loc = legend.loc
            else
              legend.loc = NULL
            
            #if(is.null(main))
            #  main = paste(colnames[1],"Performance", sep=" ")
            
            if(ylog)
              wealth.index = TRUE
            
            op <- par(no.readonly=TRUE)
            
            # The first row is the cumulative returns line plot
            par(oma = c(2, 0, 0, 0), mar=c(1,0,2,2))
                
            # The second row is the underwater plot
            par(mar=c(5,0,0,2))
    
            plot_object <- PerformanceAnalytics::chart.CumReturns(x, 
                                               main = "", 
                                               xaxis = FALSE, 
                                               legend.loc = legend.loc, 
                                               event.labels = event.labels, 
                                               ylog = ylog, 
                                               wealth.index = wealth.index, 
                                               begin = begin, 
                                               geometric = geometric, 
                                               ylab="Wealth",
                                               plot.engine = "default",
                                               ...)
              
               
               plot_object <- PerformanceAnalytics::chart.Drawdown(x, 
                                             geometric = geometric, 
                                             main = "Drawdown", 
                                             ylab = "Drawdown", 
                                             event.labels = NULL, 
                                             ylog=FALSE, 
                                             add = TRUE, 
                                             plot.engine = "default",
                                             ...)
               
               print(plot_object)
               title(main, outer = TRUE)
               par(op)
  }


plot_return_histogram <- function(rdat, scales = "free_x", bins = 30, ncol = 2){
  mdat <- melt.data.table(rdat, id.vars = "Date")
  names(mdat)[3] <- "Returns"
  ggplot(mdat, aes(x = Returns)) +  geom_histogram(bins = bins) + 
    facet_wrap( ~ variable, ncol = ncol, scales = scales) + 
    theme(legend.title=element_blank()) + xlab("") + theme_bw()
}

# https://quantstrattrader.com/2016/05/11/how-to-compute-turnover-with-return-portfolio-in-r/
